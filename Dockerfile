FROM nginx:alpine
COPY ./html /usr/share/nginx/html

# docker build -t  hello . (คำสั่งสร้าง image ชื่อ 'hello')
# docker image ls (คำสั่งตรวจสอบ image)
# docker run --name my-hello -p 86:80 -d hello (คำสั่งสร้าง container จาก image 'hello' ที่สร้าง)

# docker login registry.gitlab.com
# docker build -t registry.gitlab.com/kulniipa.bn/netka .
# docker image ls
# docker push registry.gitlab.com/kulniipa.bn/netka 

# docker tag registry.gitlab.com/kulniipa.bn/netka registry.gitlab.com/kulniipa.bn/netka:1.0
# docker image ls  
# docker push registry.gitlab.com/kulniipa.bn/netka:1.0

# docker build -t registry.gitlab.com/kulniipa.bn/netka:2.0 .
# docker push registry.gitlab.com/kulniipa.bn/netka:2.0

# docker tag registry.gitlab.com/kulniipa.bn/netka:2.0 registry.gitlab.com/kulniipa.bn/netka:latest
# docker push registry.gitlab.com/kulniipa.bn/netka:latest
